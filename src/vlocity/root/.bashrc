# .bashrc
# sourced by BASH everytime it start
## These should be set by /etc/profile
## But sometime, bash missed them. So here is the push
export PATH="/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/bin:/opt/bin:/usr/X11/bin:/usr/local/games:/usr/games"
# Set various environment variables
for SH in /etc/profile.d/*.sh; do
	   . $SH
done
#use custom keymap
xmodmap ~/.Xmodmap

## Set fancy colors
eval `dircolors -b`

## Set this for your first choice
#export BROWSER=seamonkey
export BROWSER=firefox
#export BROWSER=opera
#export EXPLORER=konqueror
#export NETWORK_EXPLORER=konqueror
#export AUDIO_PLAYER=xmms
#export VIDEO_PLAYER=gxine
export DISPLAY=$DISPLAY

# This line was appended by KDE
# Make sure our customised gtkrc file is loaded.
export GTK2_RC_FILES=$HOME/.gtkrc-2.0
export USER=vl70
