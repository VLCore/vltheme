========
VL-Theme
========

This repository tree contains the data needed by the VL-Theme SlackBuild
to generate the default desktop theming for different VectorLinux flavours.


How To Use This Repository
==========================

The ``common`` as its name suggests, contains data common to all VectorLinux
flavours.  The SlackBuild should build a package following this procedure:

#. Clone this repository
#. Theck out the appropriate tag (for version number)
#. Copy the ``common`` directory to the package DESTDIR
#. Copy the customized data for the distro Flavour being built.


Contributing to this repository
===============================

All contributions to this repository should abide by the guidelines established
above.  
